import { css } from "lit-element";

export default css`
header {
            display: flex;
            width: calc(100% - 30px);
            align-items: center;
            justify-content: space-between;
            background-color: blue;
            color: white; 
            padding: 15px;
        }
`;