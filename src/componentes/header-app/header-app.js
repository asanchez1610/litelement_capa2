import { LitElement, html, css } from 'lit-element';
import style from './header-app-style.js';

class HeaderApp extends LitElement {
    static get styles() {
        return [ style ]
    }

    static get properties() {
        return { 
            titulo: { type: String }
         };
    }

    constructor() {
        super();
        this.titulo = 'Mi Aplicacion'; 
    }

    render() {
        return html`
        <header>
            <section>
                ${this.titulo}    
            </section>
            <section>
                Opciones
            </section>
        </header>
        `
    }
}
customElements.define('header-app', HeaderApp);