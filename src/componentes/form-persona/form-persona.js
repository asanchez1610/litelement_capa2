import { LitElement, html, css } from "lit-element";
import styles from "./form-persona-style";

class FormPersona extends LitElement {
  static get styles() {
    return [styles];
  }

  static get properties() {
    return {
      persona: Object,
    };
  }

  constructor() {
    super();
    this.persona = {};
    this.init();
  }

  async init() {
    await this.updateComplete;
    let inputs = this.shadowRoot.querySelectorAll("input");
    inputs.forEach((input) =>
      input.addEventListener("input", (e) => this.updatePersona(e))
    );
  }

  updatePersona({ target }) {
    let tmp = { ...this.persona };
    tmp[target.name] = target.value;
    this.persona = tmp;
  }

  validForm() {
    let requireds = this.shadowRoot.querySelectorAll(".required");
    let errors = 0;
    requireds.forEach((input) => {
      if (input.value.length === 0) {
        errors++;
        input.classList.add("invalid");
      } else {
        input.classList.remove("invalid");
      }
    });
    return errors === 0;
  }

  registrar() {
    if (this.validForm()) {
      this.dispatchEvent(
        new CustomEvent("on-person-value", {
          detail: this.persona,
          bubbles: true,
          composed: true,
        })
      );
      this.reset();
    }
  }

  reset() {
    this.persona = {};
    let requireds = this.shadowRoot.querySelectorAll(".required");
    requireds.forEach((item) => {
      item.classList.remove("invalid");
    });
  }

  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
        crossorigin="anonymous"
      />

      <main class="form-content">
        <div class="mb-3">
          <label for="nombres" class="form-label">Nombre completo</label>
          <input
            name="nombres"
            class="form-control required"
            id="nombres"
            .value="${this.persona.nombres ? this.persona.nombres : ""}"
          />
        </div>

        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input
            name="email"
            class="form-control required"
            id="email"
            .value="${this.persona.email ? this.persona.email : ""}"
          />
        </div>

        <div class="mb-3">
          <label for="telefono" class="form-label">Teléfono</label>
          <input
            name="telefono"
            class="form-control required"
            id="telefono"
            .value="${this.persona.telefono ? this.persona.telefono : ""}"
          />
        </div>

        <div class="actions-buttons">
          <button class="btn btn-secondary" @click="${this.reset}">
            Cancelar
          </button>
          <button class="btn btn-primary" @click="${this.registrar}">
            Registrar
          </button>
        </div>
      </main>
    `;
  }
}
customElements.define("form-persona", FormPersona);
