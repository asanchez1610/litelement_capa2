import { LitElement, html, css } from 'lit-element';
import './componentes/header-app/header-app';
import './componentes/form-persona/form-persona';
import './componentes/list-persona/list-persona';

export class AppLitelementCapa2 extends LitElement {
  static get properties() {
    return {
        persona: Object
    };
  }

  static get styles() {
    return css`
      main {
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
      }
      section {
          padding: 15px;
        }
      section.form {
        width: 35%;
      }

      section.list {
        width: 65%;
      }  

      @media screen and (max-width: 768px) {
        
        main {
        flex-direction: column;
      }
        
        section.form {
        width: calc(100% - 30px);
      }

      section.list {
        width: calc(100% - 30px);
      } 
      }
    `;
  }

  constructor() {
    super();
    this.persona = {nombres: "dfgdfg", email: "fdgfd", telefono: "fdgfd"};
  }

  addPersona(persona) {
    console.log('la persona a registrar', persona);
    let listPersona = this.shadowRoot.querySelector('list-persona');
    let tmp = [ ...listPersona.personas ];
    tmp.push(persona);
    listPersona.personas = tmp;
  }

  prueba() {
    this.shadowRoot.querySelector('form-persona').persona = this.persona;
  }

  render() {
    return html`
    <header-app titulo="Aplicacion en Lit Element"></header-app>
      <main>
      <button @click="${this.prueba}" >Prueba</button>
        <section class="form">
          <form-persona
          
          @on-person-value="${({detail}) => this.addPersona(detail)}"
          ></form-persona>
        </section>

        <section class="list">
          <list-persona></list-persona>
        </section>

      </main>

    `;
  }
}
